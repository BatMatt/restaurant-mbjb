<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('restaurant_mbjb', 'Configuration/TypoScript', 'restaurant_mbjb');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_restaurantmbjb_domain_model_menu', 'EXT:restaurant_mbjb/Resources/Private/Language/locallang_csh_tx_restaurantmbjb_domain_model_menu.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_restaurantmbjb_domain_model_menu');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_restaurantmbjb_domain_model_dish', 'EXT:restaurant_mbjb/Resources/Private/Language/locallang_csh_tx_restaurantmbjb_domain_model_dish.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_restaurantmbjb_domain_model_dish');

    }
);
