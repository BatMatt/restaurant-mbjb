<?php
namespace Restaurant\RestaurantMbjb\Domain\Model;


/***
 *
 * This file is part of the "restaurant_mbjb" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Mattis BETOURNE <mattis.betourne@gmail.com>
 *
 ***/
/**
 * Menu
 */
class Menu extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * entries
     * 
     * @var
     */
    protected $entries = null;

    /**
     * mainDishes
     * 
     * @var
     */
    protected $mainDishes = null;

    /**
     * cheeses
     * 
     * @var
     */
    protected $cheeses = null;

    /**
     * desserts
     * 
     * @var
     */
    protected $desserts = null;

    /**
     * drinks
     * 
     * @var
     */
    protected $drinks = null;

    /**
     * mattis
     * 
     * @var
     */
    protected $mattis = null;

    /**
     * Returns the entries
     * 
     * @return  $entries
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Sets the entries
     * 
     * @param string $entries
     * @return void
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;
    }

    /**
     * Returns the mainDishes
     * 
     * @return  $mainDishes
     */
    public function getMainDishes()
    {
        return $this->mainDishes;
    }

    /**
     * Sets the mainDishes
     * 
     * @param string $mainDishes
     * @return void
     */
    public function setMainDishes($mainDishes)
    {
        $this->mainDishes = $mainDishes;
    }

    /**
     * Returns the cheeses
     * 
     * @return  $cheeses
     */
    public function getCheeses()
    {
        return $this->cheeses;
    }

    /**
     * Sets the cheeses
     * 
     * @param string $cheeses
     * @return void
     */
    public function setCheeses($cheeses)
    {
        $this->cheeses = $cheeses;
    }

    /**
     * Returns the desserts
     * 
     * @return  $desserts
     */
    public function getDesserts()
    {
        return $this->desserts;
    }

    /**
     * Sets the desserts
     * 
     * @param string $desserts
     * @return void
     */
    public function setDesserts($desserts)
    {
        $this->desserts = $desserts;
    }

    /**
     * Returns the drinks
     * 
     * @return  $drinks
     */
    public function getDrinks()
    {
        return $this->drinks;
    }

    /**
     * Sets the drinks
     * 
     * @param string $drinks
     * @return void
     */
    public function setDrinks($drinks)
    {
        $this->drinks = $drinks;
    }

    /**
     * Returns the mattis
     * 
     * @return  $mattis
     */
    public function getMattis()
    {
        return $this->mattis;
    }

    /**
     * Sets the mattis
     * 
     * @param string $mattis
     * @return void
     */
    public function setMattis($mattis)
    {
        $this->mattis = $mattis;
    }
}
