<?php
namespace Restaurant\RestaurantMbjb\Domain\Model;


/***
 *
 * This file is part of the "restaurant_mbjb" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Mattis BETOURNE <mattis.betourne@gmail.com>
 *
 ***/
/**
 * Dish
 */
class Dish extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
}
