<?php
namespace Restaurant\RestaurantMbjb\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Mattis BETOURNE <mattis.betourne@gmail.com>
 */
class DishTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Restaurant\RestaurantMbjb\Domain\Model\Dish
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Restaurant\RestaurantMbjb\Domain\Model\Dish();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
