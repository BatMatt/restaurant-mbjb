<?php
namespace Restaurant\RestaurantMbjb\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Mattis BETOURNE <mattis.betourne@gmail.com>
 */
class MenuTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Restaurant\RestaurantMbjb\Domain\Model\Menu
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Restaurant\RestaurantMbjb\Domain\Model\Menu();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getEntriesReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setEntriesForSetsEntries()
    {
    }

    /**
     * @test
     */
    public function getMainDishesReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setMainDishesForSetsMainDishes()
    {
    }

    /**
     * @test
     */
    public function getCheesesReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setCheesesForSetsCheeses()
    {
    }

    /**
     * @test
     */
    public function getDessertsReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setDessertsForSetsDesserts()
    {
    }

    /**
     * @test
     */
    public function getDrinksReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setDrinksForSetsDrinks()
    {
    }

    /**
     * @test
     */
    public function getMattisReturnsInitialValueFor()
    {
    }

    /**
     * @test
     */
    public function setMattisForSetsMattis()
    {
    }
}
